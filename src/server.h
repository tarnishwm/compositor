#ifndef _TARN_SERVER_H
#define _TARN_SERVER_H

#include <wayland-server-core.h>
#include <wlr/backend.h>
#include <wlr/render/wlr_renderer.h>
#include <wlr/types/wlr_compositor.h>
#include <wlr/types/wlr_output.h>
#include <wlr/types/wlr_output_layout.h>
#include <wlr/types/wlr_data_device.h>
#include <wlr/types/wlr_xdg_shell.h>

struct tarn_server {
    struct wl_display *wl_display;
    struct wlr_backend *backend;

    struct wlr_compositor *compositor;
    struct wlr_renderer *renderer;

    struct wlr_output_layout *output_layout;
    struct wl_list outputs;
    struct wl_list views;

    struct wlr_xdg_shell *xdg_shell;

    struct wl_listener new_output;
    struct wl_listener new_xdg_surface;
};

int tarn_server_init(struct tarn_server *server);
void tarn_server_destroy(struct tarn_server *server);

#endif
