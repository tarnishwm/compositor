#include <wayland-server-core.h>
#include <wlr/types/wlr_matrix.h>
#include <wlr/types/wlr_output.h>

#include "output.h"
#include "server.h"
#include "render.h"

void on_request_frame(struct wl_listener *listener, void *data) {
    struct tarn_output *output = wl_container_of(listener, output, frame);
    struct wlr_renderer *renderer = output->server->renderer;

    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);

    if (!wlr_output_attach_render(output->wlr_output, NULL))
        return;

    int width, height;
    wlr_output_effective_resolution(output->wlr_output, &width, &height);

    wlr_renderer_begin(renderer, width, height);

    float color[4] = {1.0, 0, 0, 1.0};
    wlr_renderer_clear(renderer, color);

    struct tarn_view *view;
    wl_list_for_each_reverse(view, &output->server->views, link) {
        if (!view->mapped) {
            /* An unmapped view should not be rendered. */
            continue;
        }
        
        struct render_data rdata = {
            .output = output->wlr_output,
            .view = view,
            .renderer = renderer,
            .when = &now,
        };

        wlr_xdg_surface_for_each_surface(view->xdg_surface,
                render_surface, &rdata);
    }

    wlr_renderer_end(renderer);
    wlr_output_commit(output->wlr_output);
}

void render_surface(struct wlr_surface *surface,
        int sx, int sy, void *data) {
    /* This function is called for every surface that needs to be rendered. */
    struct render_data *rdata = data;
    struct tarn_view *view = rdata->view;
    struct wlr_output *output = rdata->output;

    /* We first obtain a wlr_texture, which is a GPU resource. wlroots
     * automatically handles negotiating these with the client. The underlying
     * resource could be an opaque handle passed from the client, or the client
     * could have sent a pixel buffer which we copied to the GPU, or a few other
     * means. We don't have to worry about this, wlroos takes care of it. */
    struct wlr_texture *texture = wlr_surface_get_texture(surface);
    if (texture == NULL)
        return;

    /* The view has a position in layout coordinates. If you have two displays,
     * one next to the other, both 1080p, a view on the rightmost display might
     * have layout coordinates of 2000, 100. We need to translate that to
     * output-local coordinates, or (2000 - 1920). */
    double ox, oy = 0;
    wlr_output_layout_output_coords(
            view->server->output_layout, output, &ox, &oy);
    ox += view->x + sx, oy += view->y + sy;

    /* We also have to apply the scale factor for HiDPI outputs. this is only
     * part of the puzzle - TODO full HiDPI support. */
    struct wlr_box box = {
        .x = ox * output->scale,
        .y = oy * output->scale,
        .width = surface->current.width * output->scale,
        .height = surface->current.height * output->scale,
    };

    /* matrix calcs :) */
    float matrix[9];
    enum wl_output_transform transform =
        wlr_output_transform_invert(surface->current.transform);
    wlr_matrix_project_box(matrix, &box, transform, 0,
            output->transform_matrix);

    /* This takes our matrix, the texture, and an alpha, and performs the actual
     * rendering on the GPU. */
    wlr_render_texture_with_matrix(rdata->renderer, texture, matrix, 1);

    /* This lets the client know that we've displayed that frame and it can
     * prepare another one now if it likes. */
    wlr_surface_send_frame_done(surface, rdata->when);
}
