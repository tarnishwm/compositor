#include <stdlib.h>

#include "output.h"
#include "server.h"
#include "render.h"

void on_output_new(struct wl_listener *listener, void *data) {
    struct tarn_server *server = wl_container_of(
            listener, server, new_output);
    struct wlr_output *wlr_output = data;

    if (!wl_list_empty(&wlr_output->modes)) {
        struct wlr_output_mode *mode =
            wl_container_of(wlr_output->modes.prev, mode, link);
        wlr_output_set_mode(wlr_output, mode);
    }

    struct tarn_output *output = calloc(1, sizeof(struct tarn_output));
    clock_gettime(CLOCK_MONOTONIC, &output->last_frame);
    output->server = server;
    output->wlr_output = wlr_output;
    wl_list_insert(&server->outputs, &output->link);

    output->destroy.notify = on_output_destroy;
    wl_signal_add(&wlr_output->events.destroy, &output->destroy);
    output->frame.notify = on_request_frame;
    wl_signal_add(&wlr_output->events.frame, &output->frame);

    wlr_output_create_global(wlr_output);
}

void on_output_destroy(struct wl_listener *listener, void *data) {
    struct tarn_output *output = wl_container_of(listener, output, destroy);
    wl_list_remove(&output->link);
    wl_list_remove(&output->destroy.link);
    wl_list_remove(&output->frame.link);
    free(output);
}

void on_new_xdg_surface(struct wl_listener *listener, void *data) {
    /* This event is raised when wlr_xdg_shell receives a new xdg surface from a
     * client, either as a toplevel (application window) or popup. */
    struct tarn_server *server =
        wl_container_of(listener, server, new_xdg_surface);
    struct wlr_xdg_surface *xdg_surface = data;
    if (xdg_surface->role != WLR_XDG_SURFACE_ROLE_TOPLEVEL)
        return;

    struct tarn_view *view =
        calloc(1, sizeof(struct tarn_view));
    view->server = server;
    view->xdg_surface = xdg_surface;

    view->map.notify = on_xdg_surface_map;
    wl_signal_add(&xdg_surface->events.map, &view->map);
    view->unmap.notify = on_xdg_surface_unmap;
    wl_signal_add(&xdg_surface->events.unmap, &view->unmap);
    view->destroy.notify = on_xdg_surface_destroy;
    wl_signal_add(&xdg_surface->events.destroy, &view->destroy);

    wl_list_insert(&server->views, &view->link);
}

void on_xdg_surface_map(struct wl_listener *listener, void *data) {
    /* Called when the surface is mapped, or ready to display on-screen. */
    struct tarn_view *view = wl_container_of(listener, view, map);
    view->mapped = true;
}

void on_xdg_surface_unmap(struct wl_listener *listener, void *data) {
    /* Called when the surface is unmapped, and should no longer be shown. */
    struct tarn_view *view = wl_container_of(listener, view, map);
    view->mapped = false;
}

void on_xdg_surface_destroy(struct wl_listener *listener, void *data) {
    /* Called when the surface is destroyed and should never be shown again */
    struct tarn_view *view = wl_container_of(listener, view, destroy);
    wl_list_remove(&view->link);
    free(view);
}
