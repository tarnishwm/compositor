#ifndef _TARN_RENDER_H
#define _TARN_RENDER_H

#include <wayland-server-core.h>

#include <wlr/backend.h>
#include <wlr/types/wlr_xdg_shell.h>

//struct tarn_output {
    //struct wlr_output *wlr_output;
    //struct tarn_server *server;
    //struct timespec last_frame;

    //struct wl_listener destroy;
    //struct wl_listener frame;

    //struct wl_list link;
//};

//struct tarn_view {
    //struct wl_list link;
    //struct tarn_server *server;
    //struct wlr_xdg_surface *xdg_surface;

    //struct wl_listener map;
    //struct wl_listener unmap;
    //struct wl_listener destroy;

    //bool mapped;
    //int x, y;
//};

struct render_data {
    struct wlr_output *output;
    struct wlr_renderer *renderer;
    struct tarn_view *view;
    struct timespec *when;
};

void render_surface(struct wlr_surface *surface, int sx, int sy, void *data);
void on_request_frame(struct wl_listener *listener, void *data);

#endif
