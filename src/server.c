#include <assert.h>
#include <stdlib.h>

#include <wlr/util/log.h>

#include "server.h"
#include "output.h"

int tarn_server_init(struct tarn_server *server) {
    server->wl_display = wl_display_create();
    assert(server->wl_display);

    server->backend = wlr_backend_autocreate(server->wl_display, NULL);
    assert(server->backend);

    server->renderer = wlr_backend_get_renderer(server->backend);
    wlr_renderer_init_wl_display(server->renderer, server->wl_display);

    server->compositor = wlr_compositor_create(server->wl_display,
            server->renderer);
    wlr_data_device_manager_create(server->wl_display);

    server->output_layout = wlr_output_layout_create();

    wl_list_init(&server->outputs);
    server->new_output.notify = on_output_new;
    wl_signal_add(&server->backend->events.new_output, &server->new_output);

    wl_list_init(&server->views);
    server->xdg_shell = wlr_xdg_shell_create(server->wl_display);
    server->new_xdg_surface.notify = on_new_xdg_surface;
    wl_signal_add(&server->xdg_shell->events.new_surface,
            &server->new_xdg_surface);

    const char *socket = wl_display_add_socket_auto(server->wl_display);
    assert(socket);

    if (!wlr_backend_start(server->backend)) {
        wlr_log(WLR_ERROR, "Failed to start backend");
        tarn_server_destroy(server);
        return 1;
    }

    wlr_log(WLR_INFO, "Running compositor on WAYLAND_DISPLAY=%s", socket);
    setenv("WAYLAND_DISPLAY", socket, true);

    return 0;
}

void tarn_server_destroy(struct tarn_server *server) {
    wl_display_destroy_clients(server->wl_display);
    wl_display_destroy(server->wl_display);
}

void on_new_xdg_surface(struct wl_listener *listener, void *data);
