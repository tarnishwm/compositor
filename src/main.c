#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

#include <wayland-server-core.h>

#include "server.h"
#include "output.h"
#include "render.h"
#include <wlr/util/log.h>

int main(int argc, char **argv) {
    struct tarn_server server;

    int result = tarn_server_init(&server);
    if (result != 0)
        return result;

    wl_display_run(server.wl_display);

    tarn_server_destroy(&server);
    return 0;
}
