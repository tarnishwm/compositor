#ifndef _TARN_OUTPUT_H
#define _TARN_OUTPUT_H

#include <wayland-server-core.h>

#include <wlr/backend.h>
#include <wlr/types/wlr_xdg_shell.h>

struct tarn_output {
    struct wlr_output *wlr_output;
    struct tarn_server *server;
    struct timespec last_frame;

    struct wl_listener destroy;
    struct wl_listener frame;

    struct wl_list link;
};

struct tarn_view {
    struct wl_list link;
    struct tarn_server *server;
    struct wlr_xdg_surface *xdg_surface;

    struct wl_listener map;
    struct wl_listener unmap;
    struct wl_listener destroy;

    bool mapped;
    int x, y;
};

void on_output_new(struct wl_listener *listener, void *data);
void on_output_destroy(struct wl_listener *listener, void *data);
void on_new_xdg_surface(struct wl_listener *listener, void *data);
void on_xdg_surface_map(struct wl_listener *listener, void *data);
void on_xdg_surface_unmap(struct wl_listener *listener, void *data);
void on_xdg_surface_destroy(struct wl_listener *listener, void *data);

#endif
